# Define upstream block to specify the backend server (Rails app)
upstream rails_server {
    server localhost:3000;  # Assuming your Rails app runs on localhost:3000
}

# HTTP server block to define server settings
server {
    listen 80;  # Listen on port 80

    # Server name
    server_name localhost;  # Replace with your domain name if applicable

    # Location block to define reverse proxy settings
    location / {
        proxy_pass http://rails_server;  # Pass requests to the upstream Rails server
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
    }

    # Error handling
    error_page 500 502 503 504 /50x.html;
    location = /50x.html {
        root html;
    }
}
