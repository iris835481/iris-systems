# Dockerfile for database container (e.g., PostgreSQL)
FROM postgres:latest

# Set environment variables
ENV POSTGRES_USER=myuser
ENV POSTGRES_PASSWORD=mypassword
ENV POSTGRES_DB=mydatabase

# Expose the default PostgreSQL port (not exposed to host)
EXPOSE 5432
