// Load the file system module
const fs = require('fs');

// Read the package.json file of the Express module
const expressPackage = JSON.parse(fs.readFileSync('./node_modules/express/package.json', 'utf-8'));

// Print the version of Express
console.log("Express version:", expressPackage.version);
