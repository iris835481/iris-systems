const { MongoClient } = require('mongodb');

// MongoDB connection URI
const uri = 'mongodb://localhost:8081/admin';

// Connect to MongoDB
MongoClient.connect(uri, (err, client) => {
  if (err) {
    console.error('Failed to connect to MongoDB:', err);
    return;
  }

  console.log('Connected to MongoDB');

  // Perform operations on the database using the client object
  const db = client.db();

  // Example: Insert a document into a collection
  const collection = db.collection('system.users');
  collection.insertOne({ john: 25 }, (err, result) => {
    if (err) {
      console.error('Failed to insert document:', err);
      return;
    }
    console.log('Document inserted:', result.ops[0]);
  });

  // Close the MongoDB connection
  client.close();
});
